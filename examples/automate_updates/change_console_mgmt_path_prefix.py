import mbsd_helper

from mbsd.models import Broker, Service


def handle_activemq(broker):
    service = Service.objects.get(broker=broker, type="console")
    service.extra = '{"path": "/admin/"}'
    service.full_clean()
    service.save()
    service = Service.objects.get(broker=broker, type="jmx4perl")
    service.extra = '{"prefix": "/j4p/"}'
    service.full_clean()
    service.save()


def handle_apollo(broker):
    service = Service.objects.get(broker=broker, type="console")
    service.extra = '{"path": "/console/index.html"}'
    service.full_clean()
    service.save()
    service = Service.objects.get(broker=broker, type="mgmt")
    service.extra = '{"prefix": "/"}'
    service.full_clean()
    service.save()


def handle_rabbitmq(broker):
    service = Service.objects.get(broker=broker, type="console")
    service.extra = '{"path": "/"}'
    service.full_clean()
    service.save()
    service = Service.objects.get(broker=broker, type="mgmt")
    service.extra = '{"prefix": "/"}'
    service.full_clean()
    service.save()


def work():
    brokers = Broker.objects.filter()
    for broker in brokers:
        s_name = broker.software_name.lower()
        if s_name == "activemq":
            handle_activemq(broker)
        elif s_name == "apollo":
            handle_apollo(broker)
        elif s_name == "rabbitmq":
            handle_rabbitmq(broker)
        else:
            raise ValueError(
                "Unexpected broker software %s for broker %s" %
                (s_name, broker))

if __name__ == "__main__":
    work()
