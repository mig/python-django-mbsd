import os
import sys

# add script's folder to path
sys.path.append(os.path.dirname(__file__))
# set DJANGO_SETTINGS_MODULE
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
if django.VERSION < (1, 4):
    from django.core.management import setup_environ
    settings = __import__(os.environ["DJANGO_SETTINGS_MODULE"])
    setup_environ(settings)

# add parent folder to path in order to import mbsd module if not installed
sys.path.insert(0, os.path.join(os.getcwd(), os.pardir))
