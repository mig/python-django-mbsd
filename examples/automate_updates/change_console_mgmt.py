import mbsd_helper

from mbsd.models import Broker, Service


def handle_activemq(broker):
    defaults = {
        "type": "console",
        "protocol": "tcp",
        "port": 443,
        "ssl": True,
        "external": False,
        "extra": '{"path": "/admin"}', }
    service, created = Service.objects.get_or_create(
        broker=broker, name="console", defaults=defaults)
    if created:
        print("created console service for: %s" % (broker, ))
    service = Service.objects.get(broker=broker, type="jmx4perl")
    service.extra = '{"path": "/j4p"}'
    service.full_clean()
    service.save()


def handle_apollo(broker):
    service = Service.objects.get(broker=broker, type="mgmt", ssl=False)
    service.extra = '{"path": "/"}'
    service.full_clean()
    service.save()
    service = Service.objects.get(broker=broker, type="mgmt", ssl=True)
    service.name = "console"
    service.type = "console"
    service.extra = '{"path": "/console"}'
    service.full_clean()
    service.save()


def handle_rabbitmq(broker):
    service = Service.objects.get(broker=broker, type="mgmt", ssl=False)
    service.extra = '{"path": "/"}'
    service.full_clean()
    service.save()
    defaults = {
        "type": "console",
        "protocol": "tcp",
        "port": service.port,
        "ssl": service.ssl,
        "external": service.external,
        "extra": service.extra, }
    service, created = Service.objects.get_or_create(
        broker=broker, name="console", defaults=defaults)
    if created:
        print("created console service for: %s" % (broker, ))


def work():
    brokers = Broker.objects.filter()
    for broker in brokers:
        s_name = broker.software_name.lower()
        if s_name == "activemq":
            handle_activemq(broker)
        elif s_name == "apollo":
            handle_apollo(broker)
        elif s_name == "rabbitmq":
            handle_rabbitmq(broker)
        else:
            raise ValueError(
                "Unexpected broker software %s for broker %s" %
                (s_name, broker))

if __name__ == "__main__":
    work()
