Name:		python-django-mbsd
Version:	2.4
Release:	1%{?dist}
Summary:	MBSD - Messaging Broker Service Definition
Group:		Development/Libraries
License:	ASL 2.0
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	python python-devel
Requires:	httpd mod_wsgi MySQL-python python-defusedxml python-lxml

%description
MBSD - Messaging Broker Service Definition.

%prep
%setup -q -n %{name}-%{version}

%build 
%{__python} setup.py build
mkdir venv
pushd venv
tar xzf ../venv.tgz
popd

%install
rm -fr $RPM_BUILD_ROOT
%{__python} setup.py install --skip-build --root $RPM_BUILD_ROOT
cp -fr venv $RPM_BUILD_ROOT%{python_sitelib}/mbsd/

%check

%clean
rm -fr $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc LICENSE README CHANGES examples
/etc/mbsd/mbsd.wsgi
%config /etc/mbsd/mbsd.conf.example
%config /etc/httpd/conf.d/mbsd.vhost.conf.example
%{python_sitelib}/mbsd
%{python_sitelib}/*.egg-info
