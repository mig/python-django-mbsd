sources:
	python setup.py sdist
	mv dist/*.tar.gz .
	rm -fr dist

clean:
	@rm -fr build dist docs/_build MANIFEST python-django-mbsd-*.tar.gz *.egg-info
	@find . -name \*.pyc -print -delete
	@find . -name \*.pyo -print -delete

check:
	@pylint mbsd
