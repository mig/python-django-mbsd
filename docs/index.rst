.. mbsd documentation master file, created by
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

mbsd's documentation
====================

.. automodule:: mbsd

Contents:

.. toctree::
   :maxdepth: 1

   models
   rest_api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

