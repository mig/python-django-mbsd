
Database Model
==============

.. automodule:: mbsd.models

Application
-----------
.. autoclass:: mbsd.models.Application
    :members:

Broker
------
.. autoclass:: mbsd.models.Broker
    :members:

Client
------
.. autoclass:: mbsd.models.Client
    :members:

Cluster
-------
.. autoclass:: mbsd.models.Cluster
    :members:

Contact
-------
.. autoclass:: mbsd.models.Contact
    :members:

Group
-----
.. autoclass:: mbsd.models.Group
    :members:

Host
----
.. autoclass:: mbsd.models.Host
    :members:

Security Group
--------------
.. autoclass:: mbsd.models.SecGroup
    :members:

Service
-------
.. autoclass:: mbsd.models.Service
    :members:

SubCluster
----------
.. autoclass:: mbsd.models.SubCluster
    :members:

User
----
.. autoclass:: mbsd.models.User
    :members:

Regular Expressions
-------------------

Notes:

- ``{name}`` matches any destination name (e.g. ``foo``)
- ``{path}`` matches any destination path (e.g. ``foo.bar``)

.. autodata:: mbsd.models.REGEX_CONSUMER_TAG

.. autodata:: mbsd.models.REGEX_CONTACT

.. autodata:: mbsd.models.REGEX_DESTINATION

.. autodata:: mbsd.models.REGEX_DNS

.. autodata:: mbsd.models.REGEX_DNS_LOWER

.. autodata:: mbsd.models.REGEX_NAME

.. autodata:: mbsd.models.REGEX_NAME_LOWER

.. autodata:: mbsd.models.REGEX_SERVICE_NAME

.. autodata:: mbsd.models.REGEX_TITLE

.. autodata:: mbsd.models.REGEX_VERSION
