""" URL mappings. """
from django.conf.urls import include, url
from django.views.generic import RedirectView
from mbsd.api.resources import V1_API
from shibsso.views import login, logout

from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # admin interface
    url(r'^admin/', include(admin.site.urls)),
    # shibsso login/logout pages
    url(r'^accounts/login/$', login),
    url(r'^accounts/logout/$', logout),
    # rest api
    url(r'^api/', include(V1_API.urls)),
]
