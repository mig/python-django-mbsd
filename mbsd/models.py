"""
Model description.

ER schema:

- :download:`pdf <attachments/mbsd.pdf>` version
- :download:`mwb <attachments/mbsd.mwb>` version (to be opened with
  `MySQL Workbench <http://dev.mysql.com/downloads/workbench/>`_)

"""
import copy
import re
try:
    import json
except ImportError:
    import simplejson as json

from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator, MaxValueValidator

from mbsd.utils import get_next_copy_name

_DNAME = r'[a-zA-Z0-9\_\-]+|\{name\}'
_DPATH = r'((%s)\.)*(%s)' % (_DNAME, _DNAME, )

REGEX_CONSUMER_TAG = r'^(%s)$' % (_DNAME, )
VALIDATOR_CONSUMER_TAG = RegexValidator(re.compile(REGEX_CONSUMER_TAG))

REGEX_CONTACT = r'^([a-z0-9]+[\=\-\+\.\@])+[a-z0-9]+$'
VALIDATOR_CONTACT = RegexValidator(re.compile(REGEX_CONTACT))

REGEX_DESTINATION = r'^((%s)(\.\{path\})?|\{path\}\.(%s))$' % (_DPATH, _DPATH, )
VALIDATOR_DESTINATION = RegexValidator(re.compile(REGEX_DESTINATION))

REGEX_DNS = r'^([a-zA-Z0-9]+[\-\.])*[a-zA-Z0-9]+$'
VALIDATOR_DNS = RegexValidator(re.compile(REGEX_DNS))

REGEX_DNS_LOWER = r'^([a-z0-9]+[\-\.])*[a-z0-9]+$'
VALIDATOR_DNS_LOWER = RegexValidator(re.compile(REGEX_DNS_LOWER))

REGEX_NAME = r'^([a-zA-Z0-9]+-)*[a-zA-Z0-9]+$'
VALIDATOR_NAME = RegexValidator(re.compile(REGEX_NAME))

REGEX_NAME_LOWER = r'^([a-z0-9]+-)*[a-z0-9]+$'
VALIDATOR_NAME_LOWER = RegexValidator(re.compile(REGEX_NAME_LOWER))

REGEX_SERVICE_NAME = r'^[a-z0-9]+(\+[a-z0-9]+)?$'
VALIDATOR_SERVICE_NAME = RegexValidator(re.compile(REGEX_SERVICE_NAME))

REGEX_TITLE = r'^[a-zA-Z0-9\+\-\*\/_()., ]+$'
VALIDATOR_TITLE = RegexValidator(re.compile(REGEX_TITLE))

REGEX_VERSION = r'^([a-zA-Z0-9]+[\-\.\_])*[a-zA-Z0-9]+$'
VALIDATOR_VERSION = RegexValidator(re.compile(REGEX_VERSION))


def _json_validator(value):
    """
    Check if given value is valid JSON.
    """
    try:
        json.loads(value)
    except ValueError:
        raise ValidationError(u"not valid json: %s" % value)


def _extra_to_dict(value):
    """
    Transforms a key-value pair list separated by new lines in a dictionary.
    """
    if not value:
        return dict()
    try:
        return json.loads(value)
    except ValueError:
        raise ValidationError(u"not valid json: %s" % value)


class Contact(models.Model):
    """ Contact object. """
    title = models.CharField(max_length=128, unique=True, db_index=True,
                             validators=[VALIDATOR_TITLE, ])
    """ 128 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_TITLE`.
    """
    email = models.CharField(max_length=128, unique=True, db_index=True,
                             validators=[VALIDATOR_CONTACT, ])
    """ 128 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_CONTACT`.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return self.title

    class Meta:
        """ Customize object's meta information. """
        ordering = ["title", ]
        permissions = (
            ("view_contact", "Can see contacts"),
        )


class Cluster(models.Model):
    """ Cluster object. """
    name = models.CharField(max_length=64, unique=True, db_index=True,
                            validators=[VALIDATOR_NAME_LOWER, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        unique, matches :py:const:`REGEX_NAME_LOWER`.
    """
    title = models.CharField(max_length=255, unique=True, db_index=True,
                             validators=[VALIDATOR_TITLE, ])
    """ 255 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_TITLE`.
    """
    contact = models.ForeignKey(Contact)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Contact` element.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return self.name

    class Meta:
        """ Customize object's meta information. """
        ordering = ["name"]
        permissions = (
            ("view_cluster", "Can see cluster"),
        )


class SubCluster(models.Model):
    """
    Subcluster object.

    Primary key: :py:attr:`cluster` + :py:attr:`name`
    """
    cluster = models.ForeignKey(Cluster)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Cluster` element.
    """
    name = models.CharField(max_length=64, db_index=True,
                            validators=[VALIDATOR_NAME_LOWER, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_NAME_LOWER`.
    """
    title = models.CharField(max_length=64, validators=[VALIDATOR_TITLE, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_TITLE`.
    """
    dns = models.CharField(max_length=255, blank=True, null=True,
                           validators=[VALIDATOR_DNS_LOWER, ])
    """ 255 characters long :py:class:`django.db.models.CharField`,
        can be null, matches :py:const:`REGEX_DNS_LOWER`.
    """
    contact = models.ForeignKey(Contact)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Contact` element.
    """

    def _get_globalname(self):
        """ Return subcluster's global name. """
        if self.name:
            return u"%s@%s" % (self.name, self.cluster.name)
        else:
            return u"default@%s" % (self.cluster.name, )
    globalname = property(_get_globalname)
    """ unique name " :py:attr:`name` @ :py:attr:`Cluster.name` " """

    def _get_globaltitle(self):
        """ Return subcluster's global title. """
        if self.title:
            return u"%s - %s" % (self.cluster.title, self.title)
        else:
            return u"%s - default" % (self.cluster.title, )
    globaltitle = property(_get_globaltitle)
    """ unique name " :py:attr:`title` @ :py:attr:`Cluster.title` " """

    def __unicode__(self):
        """ Return unicode representation. """
        return self.globalname

    class Meta:
        """ Customize object's meta information. """
        ordering = ["cluster", "name"]
        unique_together = (("cluster", "name"), )
        permissions = (
            ("view_subcluster", "Can see subcluster"),
        )


class Host(models.Model):
    """ Host object. """
    name = models.CharField(max_length=255, unique=True, db_index=True,
                            validators=[VALIDATOR_NAME_LOWER, ])
    """ 255 characters long :py:class:`django.db.models.CharField`,
        unique, matches :py:const:`REGEX_NAME_LOWER`.
    """
    fullname = models.CharField(max_length=255, unique=True, db_index=True,
                                validators=[VALIDATOR_DNS_LOWER, ])
    """ 255 characters long :py:class:`django.db.models.CharField`,
        unique, matches :py:const:`REGEX_DNS_LOWER`.
    """
    dns = models.CharField(max_length=255, blank=True, null=True,
                           validators=[VALIDATOR_DNS_LOWER, ])
    """ 255 characters long :py:class:`django.db.models.CharField`,
        can be null, matches :py:const:`REGEX_DNS_LOWER`.
    """
    extra = models.TextField(blank=True, validators=[_json_validator, ])
    """ :py:class:`django.db.models.TextField` containing valid JSON. """

    def __unicode__(self):
        """ Return unicode representation. """
        return self.name

    def _get_extra_dict(self):
        return _extra_to_dict(self.extra)
    extra_dict = property(_get_extra_dict)

    def clone_it(self, new_name=None):
        """ Clone itself and return new reference. """
        old_host = copy.copy(self)
        if new_name is None:
            new_name = self.name
            while True:
                new_name = get_next_copy_name(new_name)
                if len(Host.objects.filter(name=new_name)) == 0:
                    break
        self.pk = None
        self.name = new_name
        if old_host.name in self.fullname:
            self.fullname = self.fullname.replace(old_host.name, new_name)
        else:
            while True:
                self.fullname = get_next_copy_name(self.fullname)
                if len(Host.objects.filter(fullname=self.fullname)) == 0:
                    break
        self.dns = None
        self.save()
        # Let's clone the childrens
        old_brokers = Broker.objects.filter(host=old_host)
        for broker in old_brokers:
            cloned = broker.clone_it(self)
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["name"]
        permissions = (
            ("view_host", "Can see host"),
        )


class Broker(models.Model):
    """
    Broker object.

    Primary key: :py:attr:`host` + :py:attr:`name`
    """
    host = models.ForeignKey(Host)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Host` element.
    """
    name = models.CharField(max_length=64, db_index=True,
                            validators=[VALIDATOR_NAME_LOWER, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_NAME_LOWER`.
    """
    subcluster = models.ForeignKey(SubCluster)
    """ :py:class:`django.db.models.ForeignKey` to a
        :py:class:`SubCluster` element.
    """
    software_name = models.CharField(
        max_length=64, validators=[VALIDATOR_DNS, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_DNS`.
    """
    software_version = models.CharField(
        max_length=64, validators=[VALIDATOR_VERSION, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_VERSION`.
    """
    extra = models.TextField(blank=True, validators=[_json_validator, ])
    """ :py:class:`django.db.models.TextField` containing valid JSON. """

    def _get_globalname(self):
        """ Return broker's global name. """
        return u"%s@%s" % (self.name, self.host)
    globalname = property(_get_globalname)
    """ unique name " :py:attr:`name` @ :py:attr:`host` " """

    def _get_software(self):
        """ Return broker's software. """
        return u"%s %s" % (self.software_name, self.software_version)
    software = property(_get_software)
    """ software name + version
        " :py:attr:`software_name` @ :py:attr:`software_version` " """

    def _get_extra_dict(self):
        return _extra_to_dict(self.extra)
    extra_dict = property(_get_extra_dict)

    def __unicode__(self):
        """ Return unicode representation. """
        return self.globalname

    def clone_it(self, host=None):
        """ Clone itself and return new reference. """
        old_broker = copy.copy(self)
        self.pk = None
        if host is not None:
            self.host = host
        else:
            while True:
                self.name = get_next_copy_name(self.name)
                if len(Broker.objects.filter(host=self.host,
                                             name=self.name)) == 0:
                    break
        self.save()
        # Let's clone the attached services
        old_services = Service.objects.filter(broker=old_broker)
        for service in old_services:
            cloned = service.clone_it(self)
        # Let's clone the attached applications
        old_applications = Application.objects.filter(broker=old_broker)
        for application in old_applications:
            cloned = application.clone_it(broker=self)
        # Let's clone the attached security groups
        old_secgroups = SecGroup.objects.filter(broker=old_broker)
        for secgroup in old_secgroups:
            cloned = secgroup.clone_it(broker=self)
        # Let's clone the local users
        old_users = User.objects.filter(broker=old_broker)
        for user in old_users:
            cloned = user.clone_it(broker=self)
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["host", "name"]
        unique_together = (("host", "name"), )
        permissions = (
            ("view_broker", "Can see broker"),
        )


class Service(models.Model):
    """
    Service object.

    Primary key: :py:attr:`broker` + :py:attr:`name`
    """
    SERVICE_TYPE = (
        ("amqp", "amqp"),
        ("console", "console"),
        ("jetty", "jetty"),
        ("jmx", "jmx"),
        ("jmx4perl", "jmx4perl"),
        ("mgmt", "mgmt"),
        ("openwire", "openwire"),
        ("stomp", "stomp"),
        ("ws", "ws"),
    )
    """ Allowed service types. """
    PROTOCOL_TYPE = (
        ("tcp", "tcp"),
        ("udp", "udp"),
    )
    """ Allowed protocol types. """
    broker = models.ForeignKey(Broker)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Broker` element.
    """
    name = models.CharField(max_length=64, db_index=True,
                            validators=[VALIDATOR_SERVICE_NAME, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_DNS_LOWER`.
    """
    type = models.CharField(max_length=64, db_index=True,
                            choices=SERVICE_TYPE)
    """ One of: :py:const:`SERVICE_TYPE`. """
    protocol = models.CharField(max_length=32, db_index=True,
                                choices=PROTOCOL_TYPE)
    """ One of: :py:const:`PROTOCOL_TYPE`. """
    port = models.PositiveIntegerField(validators=[MaxValueValidator(65535)])
    """ :py:class:`django.db.models.PositiveIntegerField`,
        maximum value is 65535.
    """
    ssl = models.BooleanField(default=False)
    """ :py:class:`django.db.models.BooleanField` which indicates if the
        service requires encryption.
    """
    external = models.BooleanField(default=False)
    """ :py:class:`django.db.models.BooleanField` which indicates if the
        service is exposed to the internet network.
    """
    extra = models.TextField(blank=True, validators=[_json_validator, ])
    """ :py:class:`django.db.models.TextField` containing valid JSON. """

    def _get_extra_dict(self):
        return _extra_to_dict(self.extra)
    extra_dict = property(_get_extra_dict)

    def __unicode__(self):
        """ Return unicode representation. """
        return u"%s - %s" % (self.broker, self.name)

    def clone_it(self, broker=None):
        """ Clone itself and return new reference. """
        old_service = copy.copy(self)
        self.pk = None
        if broker is not None:
            self.broker = broker
        else:
            while True:
                self.name = get_next_copy_name(self.name)
                if len(Service.objects.filter(broker=self.broker,
                                              name=self.name)) == 0:
                    break
        self.save()
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["broker", "name"]
        unique_together = (("broker", "name"), )
        permissions = (
            ("view_service", "Can see service"),
        )


class User(models.Model):
    """
    User object.

    Primary key: :py:attr:`broker` (if present) + :py:attr:`name`
    """
    broker = models.ForeignKey(Broker, blank=True, null=True)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Broker` element.
    """
    name = models.CharField(max_length=64, db_index=True,
                            validators=[VALIDATOR_DNS, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_DNS`.
    """
    password = models.CharField(max_length=255, blank=True, null=True)
    """ 255 characters long :py:class:`django.db.models.CharField`,
        can be empty if :py:attr:`dn` is provided.
    """
    dn = models.CharField(max_length=255, blank=True, null=True)
    """
    255 characters long :py:class:`django.db.models.CharField`,
    can be empty if :py:attr:`password` is provided.
    The DNs (Distinguished Names) should be stored in `RFC 2253
    <http://tools.ietf.org/pdf/rfc2253.pdf>`_
    format (e.g. "CN=John Doe,O=Acme Corporation,C=US"),
    see the slides from `UserDN Format in CAR
    <http://indico.cern.ch/contributionDisplay.py?contribId=35&confId=202686>`_
    for background information.
    """
    contact = models.ForeignKey(Contact)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Contact` element.
    """
    group_member = models.ManyToManyField("Group", through="UserMembership")
    """ :py:class:`django.db.models.ManyToManyField` to a
        :py:class:`Group` element.
    """

    def _get_has_password(self):
        """ Return true if the user has a password. """
        return self.password is not None and len(self.password) > 0
    has_password = property(_get_has_password)
    """ Return :py:const:`True` if a :py:attr:`password` has been given.
    """

    def _get_has_dn(self):
        """ Return true if the user has a dn. """
        return self.dn is not None and len(self.dn) > 0
    has_dn = property(_get_has_dn)
    """ Return :py:const:`True` if a :py:attr:`dn` has been given.
    """

    def _get_credential(self):
        """ Return dn or password depending on what is set. """
        if self.has_dn and self.has_password:
            "both"
        elif self.has_dn:
            return "dn"
        elif self.has_password:
            return "password"
        else:
            return "none"
    credential = property(_get_credential)
    """
    Return *both*, *dn*, *password* or *none* depending on the value
    of :py:attr:`dn` and :py:attr:`password`.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        if self.broker:
            return u"%s@%s" % (self.name, self.broker)
        else:
            return self.name

    def clone_it(self, broker=None):
        """ Clone itself and return new reference. """
        old_user = copy.copy(self)
        self.pk = None
        if broker is not None:
            self.broker = broker
        else:
            while True:
                self.name = get_next_copy_name(self.name)
                if len(User.objects.filter(broker=self.broker,
                                           name=self.name)) == 0:
                    break
        self.save()
        # Let's clone the user memberships
        user_memberships = UserMembership.objects.filter(
            user=old_user)
        for membership in user_memberships:
            cloned = membership.clone_it(user=self)
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["name"]
        unique_together = (("broker", "name"), )
        permissions = (
            ("view_user", "Can see user"),
        )


class Group(models.Model):
    """ Group object. """
    name = models.CharField(max_length=64, unique=True, db_index=True,
                            validators=[VALIDATOR_DNS, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        unique, matches :py:const:`REGEX_DNS`.
    """
    contact = models.ForeignKey(Contact)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Contact` element.
    """
    user_member = models.ManyToManyField(User, through="UserMembership")
    """ :py:class:`django.db.models.ManyToManyField` to a
        :py:class:`User` element.
    """
    group_member = models.ManyToManyField("self", symmetrical=False,
                                          through="GroupMembership")
    """ :py:class:`django.db.models.ManyToManyField` to a
        :py:class:`Group` element.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return self.name

    class Meta:
        """ Customize object's meta information. """
        ordering = ["name"]
        permissions = (
            ("view_group", "Can see group"),
        )


class SecGroup(models.Model):
    """
    Security group object.

    Primary key: :py:attr:`group` + :py:attr:`name` +
    :py:attr:`subcluster` (if present) + :py:attr:`broker` (if present)
    """
    NAME_TYPE = (
        ("administrator", "administrator"),
        ("console", "console"),
        ("jetty", "jetty"),
        ("jmx", "jmx"),
        ("monitor", "monitor"),
    )
    """ Security group allowed tags. """
    name = models.CharField(max_length=32, db_index=True,
                            choices=NAME_TYPE)
    """ One of :py:attr:`NAME_TYPE`. """
    subcluster = models.ForeignKey(SubCluster, blank=True, null=True)
    """ :py:class:`django.db.models.ForeignKey` to a
        :py:class:`SubCluster` element.
    """
    broker = models.ForeignKey(Broker, blank=True, null=True)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Broker` element.
    """
    title = models.CharField(max_length=128, validators=[VALIDATOR_TITLE, ])
    """ 128 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_TITLE`.
    """
    group = models.ForeignKey(Group)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Group` element.
    """

    def _get_globalname(self):
        """ Return security group's global name. """
        if self.subcluster:
            return u"%s - %s" % (self.name, self.subcluster)
        elif self.broker:
            return u"%s@%s" % (self.name, self.broker)
        else:
            return u"%s" % (self.name,)
    globalname = property(_get_globalname)
    """ unique name: " :py:attr:`name` - :py:attr:`subcluster` "
        if :py:attr:`subcluster` has been provided or
        " :py:attr:`name` - :py:attr:`broker` "
        if :py:attr:`broker` has been provided.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return self.globalname

    def clone_it(self, broker=None, subcluster=None):
        """ Clone itself and return new reference. """
        old_secgroup = copy.copy(self)
        self.pk = None
        if broker is not None:
            self.broker = broker
        if subcluster is not None:
            self.subcluster = subcluster
        if broker is None and subcluster is None:
            while True:
                self.name = get_next_copy_name(self.name)
                if len(SecGroup.objects.filter(broker=self.broker,
                                               subcluster=self.subcluster,
                                               name=self.name)) == 0:
                    break
        self.save()
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["name", "broker", "subcluster"]
        unique_together = (("group", "name", "broker", "subcluster"), )
        permissions = (
            ("view_secgroup", "Can see security group"),
        )


class UserMembership(models.Model):
    """ Object which models the users membershib to the groups. """
    group = models.ForeignKey(Group)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Group` element.
    """
    user = models.ForeignKey(User)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`User` element.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return u"%s -> %s" % (self.user, self.group)

    def clone_it(self, user=None, group=None):
        """ Clone itself and return new reference. """
        old_membership = copy.copy(self)
        self.pk = None
        if user is not None:
            self.user = user
        if group is not None:
            self.group = group
        self.save()
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["group", "user"]
        permissions = (
            ("view_usermembership", "Can see user membership"),
        )


class GroupMembership(models.Model):
    """ Object which models the membershib of groups of groups. """
    group_owner = models.ForeignKey(Group, related_name="owner")
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Group` element.
    """
    group_member = models.ForeignKey(Group, related_name="member")
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Group` element.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return u"%s -> %s" % (self.group_member, self.group_owner)

    class Meta:
        """ Customize object's meta information. """
        ordering = ["group_owner", "group_member"]
        permissions = (
            ("view_groupmembership", "Can see group membership"),
        )


class Client(models.Model):
    """
    Client object.

    Primary key: :py:attr:`name` + :py:attr:`type`
    """
    CLIENT_TYPE = (
        ("producer", "producer"),
        ("consumer", "consumer"),
    )
    """ Allowed :py:class:`Client` types. """
    CLIENT_DESTINATION_TYPE = (
        ("topic", "topic"),
        ("queue", "queue"),
    )
    """ Allowed :py:attr:`destination` type. """
    contact = models.ForeignKey(Contact)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Contact` element.
    """
    name = models.CharField(max_length=64, db_index=True,
                            validators=[VALIDATOR_NAME_LOWER, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_NAME_LOWER`.
    """
    title = models.CharField(max_length=128, validators=[VALIDATOR_TITLE, ])
    """ 128 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_TITLE`.
    """
    dns = models.CharField(max_length=255, blank=True, null=True,
                           validators=[VALIDATOR_DNS_LOWER, ])
    """ 255 characters long :py:class:`django.db.models.CharField`,
        can be empty, matches :py:const:`REGEX_DNS_LOWER`.
    """
    consumer_tag = models.CharField(max_length=64, blank=True, null=True,
                                    validators=[VALIDATOR_CONSUMER_TAG, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        can be empty, matches :py:const:`REGEX_CONSUMER_TAG`.
    """
    destination = models.CharField(max_length=128,
                                   validators=[VALIDATOR_DESTINATION, ])
    """ 128 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_DESTINATION`.
    """
    type = models.CharField(max_length=8, db_index=True, choices=CLIENT_TYPE)
    """ One of :py:attr:`CLIENT_TYPE`. """
    destination_type = models.CharField(max_length=5, db_index=True,
                                        choices=CLIENT_DESTINATION_TYPE)
    """ One of :py:attr:`CLIENT_DESTINATION_TYPE`. """
    selector = models.CharField(max_length=255, blank=True, null=True)
    """ 255 characters long :py:class:`django.db.models.CharField`,
        can be empty.
    """
    group = models.ForeignKey(Group)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Group` element.
    """
    extra = models.TextField(blank=True, validators=[_json_validator, ])
    """ :py:class:`django.db.models.TextField` containing valid JSON. """
    application_member = models.ManyToManyField("Application",
                                                through="ClientMembership")
    """ :py:class:`django.db.models.ManyToManyField` to an
        :py:class:`Application` element.
    """

    def _get_globalname(self):
        """ Return client's global name. """
        return u"%s-%s" % (self.name, self.type)
    globalname = property(_get_globalname)
    """ unique name " :py:attr:`name` - :py:attr:`type` " """

    def _get_extra_dict(self):
        return _extra_to_dict(self.extra)
    extra_dict = property(_get_extra_dict)

    def __unicode__(self):
        """ Return unicode representation. """
        return self.globalname

    def clone_it(self):
        """ Clone itself and return new reference. """
        old_client = copy.copy(self)
        self.pk = None
        while True:
            self.name = get_next_copy_name(self.name)
            self.title = get_next_copy_name(self.title)
            if len(Client.objects.filter(type=self.type,
                                         name=self.name)) == 0:
                break
        self.save()
        # Let's clone the client memberships
        client_memberships = ClientMembership.objects.filter(
            client=old_client)
        for membership in client_memberships:
            cloned = membership.clone_it(client=self)
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["name"]
        unique_together = (("name", "type"), )
        permissions = (
            ("view_client", "Can see client"),
        )


class Application(models.Model):
    """
    Application object.

    Primary key: :py:attr:`name` + :py:attr:`subcluster` (if present) +
    :py:attr:`broker` (if present)
    """
    APPLICATION_TYPE = (
        ("production", "production"),
        ("test", "test"),
    )
    """ Allowed :py:class:`Application` types. """
    subcluster = models.ForeignKey(SubCluster, blank=True, null=True)
    """ :py:class:`django.db.models.ForeignKey` to a
        :py:class:`SubCluster` element.
    """
    broker = models.ForeignKey(Broker, blank=True, null=True)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Broker` element.
    """
    name = models.CharField(max_length=64, db_index=True,
                            validators=[VALIDATOR_NAME_LOWER, ])
    """ 64 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_NAME_LOWER`.
    """
    title = models.CharField(max_length=128, validators=[VALIDATOR_TITLE, ])
    """ 128 characters long :py:class:`django.db.models.CharField`,
        matches :py:const:`REGEX_TITLE`.
    """
    type = models.CharField(max_length=16, default="production", db_index=True,
                            choices=APPLICATION_TYPE)
    """ One of :py:attr:`APPLICATION_TYPE`. """
    client_member = models.ManyToManyField(Client, through="ClientMembership")
    """ :py:class:`django.db.models.ManyToManyField` to a
        :py:class:`Client` element.
    """

    def _get_globalname(self):
        """ Return application's global name. """
        if self.subcluster:
            return u"%s - %s" % (self.name, self.subcluster)
        elif self.broker:
            return u"%s@%s" % (self.name, self.broker)
        else:
            return u"%s" % (self.name,)
    globalname = property(_get_globalname)
    """ unique name: " :py:attr:`name` - :py:attr:`subcluster` "
        if :py:attr:`subcluster` has been provided or
        " :py:attr:`name` - :py:attr:`broker` "
        if :py:attr:`broker` has been provided.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return self.globalname

    def clone_it(self, broker=None, subcluster=None):
        """ Clone itself and return new reference. """
        old_application = copy.copy(self)
        self.pk = None
        if broker is not None:
            self.broker = broker
        if subcluster is not None:
            self.subcluster = subcluster
        if broker is None and subcluster is None:
            while True:
                self.name = get_next_copy_name(self.name)
                if len(Application.objects.filter(broker=self.broker,
                                                  subcluster=self.subcluster,
                                                  name=self.name)) == 0:
                    break
        self.save()
        # Let's clone the client memberships
        client_memberships = ClientMembership.objects.filter(
            application=old_application)
        for membership in client_memberships:
            cloned = membership.clone_it(application=self)
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["name", "broker", "subcluster"]
        unique_together = (("name", "broker", "subcluster"), )
        permissions = (
            ("view_application", "Can see application"),
        )


class ClientMembership(models.Model):
    """ Object which models the clients membership to applications. """
    client = models.ForeignKey(Client)
    """
    :py:class:`django.db.models.ForeignKey` to a :py:class:`Client` element.
    """
    application = models.ForeignKey(Application)
    """ :py:class:`django.db.models.ForeignKey` to a
        :py:class:`Application` element.
    """

    def __unicode__(self):
        """ Return unicode representation. """
        return u"%s - %s" % (self.application, self.client)

    def clone_it(self, application=None, client=None):
        """ Clone itself and return new reference. """
        old_membership = copy.copy(self)
        self.pk = None
        if application is not None:
            self.application = application
        if client is not None:
            self.client = client
        self.save()
        return self

    class Meta:
        """ Customize object's meta information. """
        ordering = ["client", "application"]
        permissions = (
            ("view_clientmembership", "Can see client membership"),
        )
