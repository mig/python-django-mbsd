"""
mbsd - Messaging Brokers Service Definition
"""
AUTHOR = "CERN MIG <mig@cern.ch>"
COPYRIGHT = "Copyright (C) CERN 2012-2017"
VERSION = "2.4"
DATE = "17 February 2017"
__author__ = AUTHOR
__version__ = VERSION
__date__ = DATE
