""" REST API configuration. """
from tastypie import fields
from tastypie.api import Api
from tastypie.authentication import SessionAuthentication
from mbsd.tastypie_custom import DjangoAuthorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from mbsd.models import Host, Broker, Service, User, Group, \
    Contact, Cluster, SubCluster, Application, \
    GroupMembership, UserMembership, Client, ClientMembership, \
    SecGroup


class CustomMeta(object):
    """ Custom meta class. """
    default_format = "application/json"
    allowed_methods = ['get']
    authentication = SessionAuthentication()
    authorization = DjangoAuthorization()


class ContactResource(ModelResource):
    """ Configure Contact object resource. """
    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Contact.objects.all()
        ordering = ['title', ]
        filtering = {
            'title': ALL,
            'email': ALL,
        }


class ClusterResource(ModelResource):
    """ Configure Cluster object resource. """
    contact = fields.ForeignKey(ContactResource, 'contact')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Cluster.objects.all()
        ordering = ['name', ]
        filtering = {
            'name': ALL,
            'title': ALL,
            'contact': ALL_WITH_RELATIONS,
        }


class SubClusterResource(ModelResource):
    """ Configure Subcluster object resource. """
    cluster = fields.ForeignKey(ClusterResource, 'cluster')
    contact = fields.ForeignKey(ContactResource, 'contact')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = SubCluster.objects.all()
        ordering = ['cluster', 'name']
        filtering = {
            'name': ALL,
            'title': ALL,
            'dns': ALL,
            'cluster': ALL_WITH_RELATIONS,
            'contact': ALL_WITH_RELATIONS,
        }


class HostResource(ModelResource):
    """ Configure Host object resource. """
    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Host.objects.all()
        ordering = ['name', ]
        filtering = {
            'name': ALL,
            'fullname': ALL,
        }


class BrokerResource(ModelResource):
    """ Configure Broker object resource. """
    host = fields.ForeignKey(HostResource, 'host')
    subcluster = fields.ForeignKey(SubClusterResource, 'subcluster')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Broker.objects.all()
        ordering = ['title', ]
        filtering = {
            'name': ALL,
            'title': ALL,
            'software_name': ALL,
            'software_version': ALL,
            'host': ALL_WITH_RELATIONS,
            'subcluster': ALL_WITH_RELATIONS,
        }


class ServiceResource(ModelResource):
    """ Configure Service object resource. """
    broker = fields.ForeignKey(BrokerResource, 'broker')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Service.objects.all()
        ordering = ['type', ]
        filtering = {
            'type': ALL,
            'protocol': ALL,
            'port': ALL,
            'external': ALL,
            'broker': ALL_WITH_RELATIONS,
        }


class UserResource(ModelResource):
    """ Configure User object resource. """
    broker = fields.ForeignKey(BrokerResource, 'broker', null=True)
    contact = fields.ForeignKey(ContactResource, 'contact')
    group_member = fields.ToManyField(
        'mbsd.api.resources.GroupResource', 'group_member', null=True)

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = User.objects.all()
        ordering = ['name', ]
        filtering = {
            'name': ALL,
            'dn': ALL,
            'broker': ALL_WITH_RELATIONS,
            'contact': ALL_WITH_RELATIONS,
        }


class GroupResource(ModelResource):
    """ Configure Group object resource. """
    contact = fields.ForeignKey(ContactResource, 'contact')
    user_member = fields.ToManyField(
        UserResource, 'user_member', null=True)
    group_member = fields.ToManyField(
        'self', 'group_member', null=True)

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Group.objects.all()
        ordering = ['name', ]
        filtering = {
            'name': ALL,
            'contact': ALL_WITH_RELATIONS,
            'user_member': ALL_WITH_RELATIONS,
            'group_member': ALL_WITH_RELATIONS,
        }


class GroupMembershipResource(ModelResource):
    """ Configure GroupMembership object resource. """
    group_owner = fields.ForeignKey(GroupResource, 'group_owner')
    group_member = fields.ForeignKey(GroupResource, 'group_member')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = GroupMembership.objects.all()
        filtering = {
            'group_owner': ALL_WITH_RELATIONS,
            'group_member': ALL_WITH_RELATIONS,
        }


class SecGroupResource(ModelResource):
    """ Configure SecGroup object resource. """
    group = fields.ForeignKey(GroupResource, 'group')
    broker = fields.ForeignKey(BrokerResource, 'broker', null=True)
    subcluster = fields.ForeignKey(SubClusterResource, 'subcluster', null=True)

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = SecGroup.objects.all()
        ordering = ['group', 'name']
        filtering = {
            'name': ALL,
            'title': ALL,
            'group': ALL_WITH_RELATIONS,
            'subcluster': ALL_WITH_RELATIONS,
            'broker': ALL_WITH_RELATIONS,
        }


class UserMembershipResource(ModelResource):
    """ Configure UserMembership object resource. """
    group = fields.ForeignKey(GroupResource, 'group')
    user = fields.ForeignKey(UserResource, 'user')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = UserMembership.objects.all()
        filtering = {
            'group': ALL_WITH_RELATIONS,
            'user': ALL_WITH_RELATIONS,
        }


class ClientResource(ModelResource):
    """ Configure Client object resource. """
    contact = fields.ForeignKey(ContactResource, 'contact')
    group = fields.ForeignKey(GroupResource, 'group')
    application_member = fields.ToManyField(
        'mbsd.api.resources.ApplicationResource', 'application_member')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Client.objects.all()
        ordering = ['title', ]
        filtering = {
            'name': ALL,
            'title': ALL,
            'dns': ALL,
            'consumer_tag': ALL,
            'destination': ALL,
            'type': ALL,
            'destination_type': ALL,
            'selector': ALL,
            'contact': ALL_WITH_RELATIONS,
            'group': ALL_WITH_RELATIONS,
            'application_member': ALL_WITH_RELATIONS,
        }


class ApplicationResource(ModelResource):
    """ Configure Application object resource. """
    broker = fields.ForeignKey(BrokerResource, 'broker', null=True)
    subcluster = fields.ForeignKey(SubClusterResource, 'subcluster', null=True)
    client_member = fields.ToManyField(
        ClientResource, 'client_member', null=True)

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = Application.objects.all()
        ordering = ['title', ]
        filtering = {
            'name': ALL,
            'title': ALL,
            'subcluster': ALL_WITH_RELATIONS,
            'broker': ALL_WITH_RELATIONS,
            'client_member': ALL_WITH_RELATIONS,
        }


class ClientMembershipResource(ModelResource):
    """ Configure ClientMembership object resource. """
    client = fields.ForeignKey(ClientResource, 'client')
    application = fields.ForeignKey(ApplicationResource, 'application')

    class Meta(CustomMeta):
        """ Customize meta information. """
        queryset = ClientMembership.objects.all()
        filtering = {
            'client': ALL_WITH_RELATIONS,
            'application': ALL_WITH_RELATIONS,
        }

API_RESOURCES = (
    ClusterResource, SubClusterResource, HostResource,
    BrokerResource, ServiceResource, ApplicationResource,
    ClientResource, ContactResource, UserResource, GroupResource,
    SecGroupResource, )
V1_API = Api(api_name='v1')
for resource in API_RESOURCES:
    V1_API.register(resource())
