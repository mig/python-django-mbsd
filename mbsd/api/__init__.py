"""

A REST API is provided.

How to access resources
-----------------------

- Available formats (if you access with a browser xml format is
  probably more readable than json as xml is usually prettified by browsers):

  - xml
  - json

Some examples
-------------

- list of resources available::

        /mbsd/api/v1/?format=json

- list of subclusters::

        /mbsd/api/v1/subcluster/?format=json

- subcluster's schema::

        /mbsd/api/v1/subcluster/schema/?format=json

- subcluster with ``id=1``::

        /mbsd/api/v1/subcluster/1/?format=json

- subcluster with name ``prod``::

        /mbsd/api/v1/subcluster/?name=prod&format=json

- subclusters which cluster's name is ``foo`` it is possible to filter
  through foreign key's fields using double underscore ``__`` in this example
  ``cluster__name`` corresponds to the name field of the resource cluster::

        /mbsd/api/v1/subcluster/?cluster__name=foo&format=json

- filterable fields are listed in the schema page, fields can be filtered
  with the operators::

        exact, lt, lte, gte, gt

  in the following way::

        /mbsd/api/v1/subcluster/?name__exact=prod&format=json

- elements returned are paginated, in the meta section of each page you
  can find information about pages::

        /mbsd/api/v1/<resource_name>/?offset=20&limit=20&format=json

"""
