"""
Django settings for the MBSD project.
"""

import os
import sys
from ConfigParser import RawConfigParser, NoSectionError
from django.core.exceptions import ImproperlyConfigured

APPLICATION_BASE = os.path.abspath(os.path.split(__file__)[0])
CONFIG_FILE = "/etc/mbsd/mbsd.conf"
SETTINGS_EXPORT = ["SITE_NAME"]
DEFAULT_CONFIG = {
    "SITE_NAME": "Messaging Broker Service Definition (MBSD)",
}

config = RawConfigParser(DEFAULT_CONFIG)
config.read(CONFIG_FILE)

try:
    SITE_NAME = config.get("general", "SITE_NAME")
    SECRET_KEY = config.get("general", "SECRET_KEY")
    ADMIN_NAME = config.get("general", "ADMIN_NAME")
    ADMIN_EMAIL = config.get("general", "ADMIN_EMAIL")
    if config.has_option("general", "ALLOWED_HOSTS"):
        hosts = config.get("general", "ALLOWED_HOSTS")
        ALLOWED_HOSTS = [val.strip() for val in hosts.split(",")]
    DATABASE_ENGINE = config.get("database", "DATABASE_ENGINE")
    DATABASE_USER = config.get("database", "DATABASE_USER")
    DATABASE_PASSWORD = config.get("database", "DATABASE_PASSWORD")
    DATABASE_NAME = config.get("database", "DATABASE_NAME")
    DATABASE_HOST = config.get("database", "DATABASE_HOST")
    DATABASE_PORT = config.get("database", "DATABASE_PORT")
    DEBUG_FLAG = False
    if config.get("debug", "DEBUG") == "True":
        DEBUG_FLAG = True
    DEBUG_IPS = config.get("debug", "DEBUG_IPS")
    DEBUG_IPS = tuple([val.strip() for val in DEBUG_IPS.split(",")])
except NoSectionError:
    error = sys.exc_info[1]
    raise ImproperlyConfigured(error)

DEBUG = DEBUG_FLAG
INTERNAL_IPS = DEBUG_IPS

ADMINS = (
    (ADMIN_NAME, ADMIN_EMAIL),
)
MANAGERS = ADMINS

#
LOGIN_REDIRECT_URL = '/mbsd/admin/'
LOGIN_URL = '/mbsd/accounts/login/'
AUTHENTICATION_BACKENDS = ('shibsso.backends.ShibSSOBackend', )
SHIB_SSO_ADMIN = True
SHIB_SSO_CREATE_ACTIVE = True
SHIB_SSO_CREATE_STAFF = False
SHIB_SSO_CREATE_SUPERUSER = False
SHIB_LOGIN_PATH = '/Shibboleth.sso/?target='
SHIB_LOGOUT_URL = 'https://login.cern.ch/adfs/ls/?wa=wsignout1.0&returnurl='
META_EMAIL = 'ADFS_EMAIL'
META_FIRSTNAME = 'ADFS_FIRSTNAME'
META_GROUP = 'ADFS_GROUP'
META_LASTNAME = 'ADFS_LASTNAME'
META_USERNAME = 'ADFS_LOGIN'
#

DATABASES = {
    'default': {
        'ENGINE': DATABASE_ENGINE,
        'NAME': DATABASE_NAME,
        'USER': DATABASE_USER,
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': DATABASE_HOST,
        'PORT': DATABASE_PORT,
    },
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Zurich'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# The ID, as an integer, of the current site in the django_site database table.
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(APPLICATION_BASE, 'static/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# Make this unique, and don't share it with anybody.
# SECRET_KEY = ''

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'shibsso.middleware.ShibSSOMiddleware',
    'mbsd.middleware.Http403Middleware',
)

ROOT_URLCONF = 'mbsd.urls'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'shibsso',
    #'django.contrib.auth',
    'mbsd',
    'tastypie',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# Define our test runner.
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# Define the template settings.
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(APPLICATION_BASE, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django_settings_export.settings_export',
            ],
            'debug': DEBUG_FLAG,
        },
    },
]
