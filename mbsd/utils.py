"""
Various utilities.
"""
import re

_NEXT_COPY_NAME = r'^(?P<name>.*)\.copy(?P<number>\d*)$'
_NEXT_COPY_NAME_RE = re.compile(_NEXT_COPY_NAME)


def get_next_copy_name(name):
    """
    Give a copy name:
        foo -> foo.copy
        foo.copy -> foo.copy2
        foo.copyN -> foo.copyN+1
    """
    result = _NEXT_COPY_NAME_RE.match(name)
    num = 1
    if result is not None:
        outname = result.group("name")
        try:
            num = result.group("number")
            if len(num) == 0:
                num = 2
            else:
                num = int(num) + 1
        except (IndexError, ValueError):
            pass
    else:
        outname = name
    if num == 1:
        return "%s.copy" % (outname)
    else:
        return "%s.copy%d" % (outname, num)
