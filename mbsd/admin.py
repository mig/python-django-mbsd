""" Admin module. """
import random
import string
from mbsd.models import Host, Broker, Service, User, Group, \
    Contact, Cluster, SubCluster, Application, Client, SecGroup
from django.contrib import admin

RNDCHARS = string.ascii_letters + string.digits


def clone_selected(self, request, queryset):
    """ Clone selected objects. """
    counter = 0
    elements = list()
    for element in queryset:
        old_name = str(element)
        cloned = element.clone_it()
        if cloned is not None:
            counter += 1
            elements.append("%s cloned as %s" % (old_name, str(cloned)))
    self.message_user(
        request,
        "%d elements successfully cloned: %s" % (counter, ", ".join(elements)))
clone_selected.short_description = "Clone selected objects"
setattr(admin.ModelAdmin, "clone_selected", clone_selected)


def randomize_selected(self, request, queryset):
    """ Randomize selected users passwords. """
    counter = 0
    for element in queryset:
        if not element.has_password:
            continue
        element.password = "".join(random.choice(RNDCHARS) for c in range(16))
        element.save()
        counter += 1
    self.message_user(
        request,
        "%d user passwords successfully randomized" % counter)
randomize_selected.short_description = "Randomize selected user passwords"
setattr(admin.ModelAdmin, "randomize_selected", randomize_selected)

class BrokerInline(admin.TabularInline):
    """ Inline broker editing for hosts. """
    model = Broker
    extra = 1


class HostAdmin(admin.ModelAdmin):
    """ Host object customization for admin interface. """
    list_display = ('name', 'fullname', 'dns', )
    inlines = (BrokerInline, )
    actions = ['clone_selected', ]


class ServiceInline(admin.TabularInline):
    """ Inline service editing for brokers. """
    model = Service
    extra = 1


class BrokerAdmin(admin.ModelAdmin):
    """ Broker object customization for admin interface. """
    list_display = ('globalname', 'subcluster', 'software', )
    list_filter = ['subcluster', 'software_name', ]
    inlines = (ServiceInline, )
    actions = ['clone_selected', ]


class ServiceAdmin(admin.ModelAdmin):
    """ Service object customization for admin interface. """
    list_display = (
        'broker', 'name', 'type', 'protocol', 'port', 'ssl', 'external', )
    list_filter = [
        'broker', 'name', 'type', 'protocol', 'port', 'ssl', 'external', ]
    actions = ['clone_selected', ]


class UserInline(admin.TabularInline):
    """ Inline group membership editing for users. """
    model = Group.user_member.through
    extra = 1


class UserAdmin(admin.ModelAdmin):
    """ User object customization for admin interface. """
    list_display = ('name', 'contact', 'broker', 'credential', )
    list_filter = ['contact', 'broker', ]
    search_fields = ['name', 'dn', ]
    inlines = (UserInline, )
    actions = ['clone_selected', 'randomize_selected', ]


class SecGroupAdmin(admin.ModelAdmin):
    """ SecGroup object customization for admin interface """
    list_display = ('globalname', 'title', 'group', 'subcluster', 'broker', )
    list_filter = ['name', 'group', 'subcluster', 'broker', ]
    search_fields = ['name', 'title', ]
    actions = ['clone_selected', ]


class SecGroupInline(admin.TabularInline):
    """ Inline security group management """
    model = SecGroup
    extra = 1


class GroupInline(admin.TabularInline):
    """ Inline group membership for groups. """
    model = Group.group_member.through
    fk_name = "group_owner"
    extra = 1


class GroupAdmin(admin.ModelAdmin):
    """ Group object customization for admin interface. """
    list_display = ('name', 'contact', )
    list_filter = ['contact', ]
    inlines = (SecGroupInline, UserInline, GroupInline, )
    search_fields = ['name', ]


class ContactAdmin(admin.ModelAdmin):
    """ Contact object customization for admin interface. """
    list_display = ('title', 'email', )


class ClusterInline(admin.TabularInline):
    """ Inline subcluster editing for clusters. """
    model = SubCluster
    extra = 1


class ClusterAdmin(admin.ModelAdmin):
    """ Cluster object customization for admin interface. """
    list_display = ('name', 'title', 'contact', )
    list_filter = ['contact', ]
    inlines = (ClusterInline, )


class SubclusterInline(admin.TabularInline):
    """ Inline broker editing for subclusters. """
    model = Broker
    extra = 1


class SubClusterAdmin(admin.ModelAdmin):
    """ Subcluster object customization for admin interface. """
    list_display = ('title', 'cluster', 'dns', 'contact', )
    list_filter = ['cluster', 'contact', ]
    inlines = (SubclusterInline, )


class ApplicationInline(admin.TabularInline):
    """ Inline client editing for applications. """
    model = Application.client_member.through
    extra = 1


class ApplicationAdmin(admin.ModelAdmin):
    """ Application object customization for admin interface. """
    list_display = ('globalname', 'title', 'type', 'subcluster', 'broker', )
    list_filter = ['type', 'subcluster', 'broker', ]
    search_fields = ['name', 'title', ]
    inlines = (ApplicationInline, )
    actions = ['clone_selected', ]


class ClientInline(admin.TabularInline):
    """ Inline application membership editing for clients. """
    model = Client.application_member.through
    extra = 1


class ClientAdmin(admin.ModelAdmin):
    """ Client object customization for admin interface. """
    fields = (
        'contact', 'type', 'name', 'title', 'destination_type', 'destination',
        'consumer_tag', 'selector', 'group', 'dns', 'extra', )
    list_display = ('title', 'type', 'destination_type', 'destination',
                    'consumer_tag', 'group', 'contact', )
    list_filter = ['type', 'destination_type', 'group', 'contact', ]
    search_fields = ['name', 'title', ]
    inlines = (ClientInline, )
    actions = ['clone_selected', ]

admin.site.register(Application, ApplicationAdmin)
admin.site.register(Broker, BrokerAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Cluster, ClusterAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Host, HostAdmin)
admin.site.register(SecGroup, SecGroupAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(SubCluster, SubClusterAdmin)
admin.site.register(User, UserAdmin)
