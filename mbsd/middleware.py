""" Custom middlewares. """
from django.core.urlresolvers import reverse


"""
    403 pages are supported in django 1.4, doing it by my own.
"""
from django.template import RequestContext, loader
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseForbidden


class Http403(Exception):
    """ Http403 error. """
    pass


def render_to_403(*args, **kwargs):
    """ Render 403 page. """
    if not isinstance(args, list):
        args = ['403.html', ]

    httpresponse_kwargs = {'mimetype': kwargs.pop('mimetype', None)}
    response = HttpResponseForbidden(
        loader.render_to_string(*args, **kwargs), **httpresponse_kwargs)
    return response


class Http403Middleware(object):
    """ 403 middleware. """
    def process_exception(self, request, exception):
        """ Implement exception process. """
        if isinstance(exception, PermissionDenied) or \
                isinstance(exception, Http403):
            if settings.DEBUG:
                raise exception
            return render_to_403(context_instance=RequestContext(request))
