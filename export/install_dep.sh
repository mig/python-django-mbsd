#! /bin/sh

DEPS_FOLDER="lib/"

[ ! -d ${DEPS_FOLDER} ] && mkdir ${DEPS_FOLDER}

export PYTHONPATH=${DEPS_FOLDER}

easy_install -d ${DEPS_FOLDER} -O2 $*

