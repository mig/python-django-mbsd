#!/usr/bin/python
"""
Export MBSD data.
"""
# required for django configuration
import mbsd_helper

import argparse
import filecmp
import os
import re
import json
from subprocess import Popen, PIPE
import tempfile

from django.db.models import Q
from mbsd.models import Host, Broker, Service, User, ClientMembership

PROG = "export_mbmon_cfg"
VERSION = "0.4"
AUTHOR = "Massimo Paladin <massimo.paladin@gmail.com>"
COPYRIGHT = "Copyright CERN 2013-2017"
ARGUMENTS = [
    ("out", {
        "long": "--out",
        "help": "output folder"}),
    ("prefix", {
        "long": "--prefix",
        "help": "prefix for extra data"}),
    ("help", {
        "short": "-h",
        "long": "--help",
        "action": "help",
        "help": "print the help page"}),
    ("version", {
        "short": "-v",
        "long": "--version",
        "action": "version",
        "version": "%s %s" % (PROG, VERSION),
        "help": "print the program version"}),
]

DEFAULT_OPTIONS = {
    "out": ".",
    "prefix": "mbmon",
}

CONFIG = dict()

CLEAN_RE = re.compile("[^a-z0-9\-]+", re.IGNORECASE)

DEFAULT_BROKER_EXTRA = {
    "activemq": {
        "wrapper_command": "<{BROKER}>_procstat_wrapper",
        "process_command": "<{BROKER}>_procstat_broker",
        "threads_max":     "10000",
        "consumers_max":   "10000",
        "established_max":  "3000",
    },
    "apollo": {
        "threads_max":      "1000",
        "consumers_max":    "1000",
        "established_max":  "1000",
    },
    "rabbitmq": {
        "sockets_max":           "1000",
        "processes_max":        "10000",
        "file_descriptors_max":  "1000",
        "established_max":       "1000",
    },
}

DEFAULT_CLIENT_EXTRA = {
    "check_received": {
        "start": "now-2h",
        "ds": "received_messages",
        "avg-warning":  "@0",
        "avg-critical": "@0",
    },
    "check_sent": {
        "start": "now-2h",
        "ds": "sent_messages",
        "avg-warning":  "@0",
        "avg-critical": "@0",
    },
    "check_stored": {
        "start": "now-2h",
        "ds": "stored_messages",
        "avg-warning":   "100000",
        "avg-critical": "1000000",
    },
}

DEFAULT_SERVICE_EXTRA = {
    "jmx4perl": {
        "prefix": "/j4p/",
    },
}

DEFAULT_SERVICE_GROUPS = {
    "activemq": "ActiveMQ-Base",
    "apollo":     "Apollo-Base",
    "rabbitmq": "RabbitMQ-Base",
}

CATCH_ALL = {
    "check_stored": {
        "ds": "stored_messages",
        "start": "now-2h",
        "avg-warning":  "1000000",
        "avg-critical": "3000000",
    }
}


def software_type(broker):
    """ Return the messaging software type. """
    software = broker.software_name.lower()
    if "apollo" in software:
        return "apollo"
    elif "activemq" in software:
        return "activemq"
    elif "rabbitmq" in software:
        return "rabbitmq"
    else:
        raise ValueError("unexpected broker software %s" % software)


def clean(string):
    """ Clean the given string. """
    return CLEAN_RE.sub("_", string)


def get_arguments():
    """ Return the arguments. """
    parser = argparse.ArgumentParser(
        prog=PROG,
        # description="",
        epilog="AUTHOR\n\n%s - %s" % (AUTHOR, COPYRIGHT),
        argument_default=argparse.SUPPRESS,
        formatter_class=lambda prog:
        argparse.RawDescriptionHelpFormatter(prog, max_help_position=33))
    for name, elopts in ARGUMENTS:
        if name == "help":
            continue
        t_args = list()
        t_kwargs = elopts.copy()
        if name in DEFAULT_OPTIONS:
            t_kwargs["help"] = "%s [default: %s]" % \
                (t_kwargs["help"], DEFAULT_OPTIONS[name])
        for arg in ["short", "long"]:
            if arg in t_kwargs:
                t_args.append(t_kwargs.pop(arg))
        t_kwargs.pop("positional", None)
        parser.add_argument(*t_args, **t_kwargs)
    read = vars(parser.parse_args())
    config = DEFAULT_OPTIONS.copy()
    config.update(read)
    return config


def client_destination(software, client):
    """ Return the client destination pattern. """
    destination = client.destination
    consumer_tag = client.consumer_tag
    if not consumer_tag:
        return "/%s/%s" % (client.destination_type, destination)
    if client.destination_type == "queue":
        raise ValueError("queue + consumer tag found for %s", client)
    if software == "apollo":
        client_extra = client.extra_dict
        dsub_id = client_extra.get("apollo", dict()).get("id", None)
        if dsub_id is None:
            return "/dsub/%s.%s" % (destination, consumer_tag)
        else:
            return "/dsub/%s" % dsub_id
    elif software == "activemq":
        return "/queue/Consumer.%s.%s" % (consumer_tag, destination)
    else:
        raise ValueError("queue + consumer tag unsupported for %s" % software)


def generate_clients(broker):
    """ Generate clients representation given a broker. """
    client_memberships = ClientMembership.objects.filter(
        Q(application__subcluster=broker.subcluster) |
        Q(application__broker=broker)).select_related(
            "client", "application", "client__contact")
    generated = dict()
    done = dict()
    software = software_type(broker)
    for membership in client_memberships:
        application = membership.application
        client = membership.client
        name = "%s-%s" % (client.name, client.type)
        if name in done:
            continue
        else:
            done[name] = True
        single = dict()
        single["include"] = client_destination(software, client)
        client_extra = client.extra_dict
        extra = client_extra.get(CONFIG["prefix"], dict())
        # non-production applications have no check_* tests
        if application.type != "production":
            for chk in extra.keys():
                if chk.startswith("check_"):
                    del(extra[chk])
        # add default values for the check_* tests
        for chk in extra.keys():
            if not chk in DEFAULT_CLIENT_EXTRA:
                continue
            for opt in DEFAULT_CLIENT_EXTRA[chk]:
                if opt in extra[chk]:
                    continue
                extra[chk][opt] = DEFAULT_CLIENT_EXTRA[chk][opt]
        single.update(extra)
        contact = client.contact
        if any([x.startswith("check_") for x in extra]):
            single["contacts"] = {
                clean(contact.title): {"email": contact.email}}
        generated[name] = single
    # adding hardcoded catchall
    generated["catchall"] = CATCH_ALL
    return generated


def generate_services(broker):
    """ Generate single broker services helper routine. """
    services = Service.objects.filter(broker=broker)
    generated = list()
    for service in services:
        if service.protocol != "tcp":
            continue
        if service.name[:6] == "stomp2":
            continue
        single = dict()
        single["port"] = service.port
        single["service"] = service.type
        if service.ssl:
            single["proto"] = "%s+ssl" % service.type
        else:
            single["proto"] = service.type
        single.update(service.extra_dict)
        if "mbcg" in single:
            del(single["mbcg"])
        if service.type in DEFAULT_SERVICE_EXTRA:
            for name in DEFAULT_SERVICE_EXTRA[service.type]:
                if name not in single:
                    single[name] = DEFAULT_SERVICE_EXTRA[service.type][name]
        if (not service.ssl) and service.type in ["mgmt", "console"]:
            user = User.objects.get(name="monitor", broker=broker)
            single["name"] = "monitor"
            single["pass"] = user.password
        generated.append(single)
    return generated


def generate_broker(host, broker):
    """ Generate broker representation given a broker. """
    extra = broker.extra_dict.get(CONFIG["prefix"], dict())
    generated = dict()
    generated["broker"] = broker.name
    generated["name"] = "%s.%s.msg.cern.ch" % (broker.name, host.name)
    for element in ["services", "service_groups"]:
        if element in extra:
            generated[element] = extra[element]
    generated.setdefault("service_groups",
                         DEFAULT_SERVICE_GROUPS[software_type(broker)])
    generated["tags"] = ",".join(set([
        broker.software_name,
        broker.subcluster.globalname,
        broker.subcluster.cluster.name, ]))
    contact = broker.subcluster.contact
    generated["contacts"] = {clean(contact.title): {"email": contact.email}}
    sub_extra = extra.get("extra", dict())
    sub_extra.setdefault("instance", broker.name)
    sub_extra.setdefault("type", software_type(broker))
    def_extra = DEFAULT_BROKER_EXTRA[software_type(broker)]
    for name in def_extra:
        defval = def_extra[name].replace("<{BROKER}>", broker.name)
        sub_extra.setdefault(name, defval)
    for name in ("disk_size disk_data disk_log disk_tmp " +
                 "memory_size memory_heap").split():
        if name in broker.extra_dict:
            sub_extra.setdefault(name, broker.extra_dict[name])
    generated["extra"] = sub_extra
    generated["broker-services"] = {
        "service": generate_services(broker)}
    if software_type(broker) != "rabbitmq":
        generated_clients = generate_clients(broker)
        if generated_clients:
            generated["clients"] = generated_clients
    return generated


def generate_host(host):
    """ Generate single host configuration. """
    generated = list()
    # generate system.* object
    contact = dict()
    email_done = dict()
    tags = ["system", ]
    brokers = Broker.objects.filter(
        host=host).select_related(
            "subcluster", "subcluster__contact")
    for broker in brokers:
        broker_contact = broker.subcluster.contact
        email = broker_contact.email
        if email in email_done:
            continue
        contact[clean(broker_contact.title)] = {
            "email": email, }
        email_done.setdefault(email, True)
        tags.append(broker.subcluster.globalname)
        tags.append(broker.subcluster.cluster.name)
    system = {
        "name": "system.%s.msg.cern.ch" % host.name,
        "service_groups": "MIG",
        "tags": ",".join(set(tags)),
    }
    if contact:
        system["contacts"] = contact
    generated.append(system)
    # generate brokers objects
    brokers = Broker.objects.filter(
        host=host).select_related(
            "subcluster", "subcluster__cluster", "subcluster__contact")
    for broker in brokers:
        generated.append(generate_broker(host, broker))
    return generated


# copied from mtb.conf...
def write_apache_config(path, conf):
    """ Write Apache style config files. """
    tmp, tmp_path = tempfile.mkstemp()
    tmp = open(tmp_path, "w+")
    tmp.write(json.dumps(conf))
    tmp.close()
    cgopt = "-SaveSorted => 1, -StoreDelimiter => \" = \"";
    cmd = "perl -e '" \
        "use Config::General qw(); " \
        "use JSON qw(from_json); " \
        "open(FILE, \"%s\") or die(\"Cannot open file: %s\"); " \
        "$json = join(\"\", <FILE>); " \
        "close(FILE) or die(\"Cannot close file: %s\"); " \
        "$cg = Config::General->new(%s); " \
        "$cg->save_file(\"%s\", from_json($json));'" \
        % (tmp_path, tmp_path, tmp_path, cgopt, path.replace("@", "\@"))
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
    _, err = proc.communicate()
    if err:
        raise ValueError(str(err).strip())
    os.remove(tmp_path)


def write_conf(conf, file_name):
    """ Write configuration to given file name. """
    fullpath = os.path.join(CONFIG["out"], file_name)
    temppath = fullpath + ".tmp"
    write_apache_config(temppath, {"host": conf})
    if os.path.exists(fullpath) and filecmp.cmp(fullpath, temppath):
        os.unlink(temppath)
        print("%s is up-to-date" % fullpath)
        return False
    if os.path.exists(fullpath):
        os.unlink(fullpath)
    os.rename(temppath, fullpath)
    print("%s has been updated" % fullpath)
    return True

def init():
    """ Initialize the program. """
    global CONFIG
    CONFIG = get_arguments()
    output_folder = CONFIG["out"]
    if os.path.exists(output_folder):
        if not os.path.isdir(output_folder):
            raise ValueError("out value is not a folder")
    else:
        os.makedirs(output_folder)


def work():
    """ Do the job, loop over object and generate their configuration. """
    skip_re = re.compile("^mb")
    hosts = Host.objects.all()
    counter = 0
    for host in hosts:
        if skip_re.match(host.name):
            continue
        conf = generate_host(host)
        if write_conf(conf, "%s.conf" % host.name):
            counter += 1
    print("configuration generated for %d hosts." % counter)


if __name__ == "__main__":
    init()
    work()
