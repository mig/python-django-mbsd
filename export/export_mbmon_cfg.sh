#!/bin/sh

BASEDIR=$(dirname $0)

MESSAGE=$( cat <<EOF
This script is intended to use a virtual environment in the current
folder, the expected virtual environment is \"env\".
If you want to create it follow this steps:

    virtualenv env
    source env/bin/activate
    pip install Django
    pip install argparse
    pip install MySQL-python
    deactivate

Now you can rerun this script again.
EOF
)

ACTIVATE="${BASEDIR}/env/bin/activate"
# activate virtualenv
[ ! -f ${ACTIVATE} ] && { echo "${MESSAGE}"; exit 1; }
source ${ACTIVATE}

# run export script in virtual environment
python ${BASEDIR}/export_mbmon_cfg.py $*

# deactivate the virtual environment
deactivate

