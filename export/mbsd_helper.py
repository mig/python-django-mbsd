import os
import site
import sys

BASEDIR = os.path.dirname(__file__)

# add script's folder to path
sys.path.insert(0, BASEDIR)
# deps folder if present
deps_folder = os.path.join(BASEDIR, "lib")
if os.path.exists(deps_folder) and os.path.isdir(deps_folder):
    site.addsitedir(deps_folder)
# set DJANGO_SETTINGS_MODULE
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
if django.VERSION < (1, 4):
    from django.core.management import setup_environ
    settings = __import__(os.environ["DJANGO_SETTINGS_MODULE"])
    setup_environ(settings)

# add parent folder to path in order to import mbsd module if not installed
sys.path.insert(0, os.path.join(BASEDIR, os.pardir))
